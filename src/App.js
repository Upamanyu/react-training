import logo from './logo.svg';
import './App.css';

function App() {

const printResult = (e) => {
    e.preventDefault();
    const formObject = e.target ;
    const inputs = formObject.elements;
    alert(`

        Name: ${inputs["username"].value}
        Company: ${inputs["company"].value}
        Phone no: ${inputs["phone"].value}
        Gender: ${inputs["gender"].value}
          `)
  }

  return (
    <div className="App">
      <form id="my-form" onSubmit={printResult}>
      <input type="text" name="username" placeholder="Name" />Enter your name!
      <br />
      <input type="text" name="company" />Enter company     
      <br />
      <input type="text" name="phone" />Enter phone number
      <br />
      <input type="text" name="gender" />Enter gender
      <br />
      <input type="submit" value="Click me" />
      </form>

    </div>
  );
}

export default App;
